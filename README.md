# ships!

## wat?
- This is a game of battleships made in CSS only
- There's loads of HTML involved, so i retreated to pug. Also, less.

## why?
- Because i can

## How to play
- It's a local multiplayer, so you need to pass around a screen or something
- the current player is always turquise (or @playerColor)
- the current enemy is always pink (or @enemyColor)
- there are no hardcoded rules how many or in which fashion ships can be placed
- there is also no hard coded win condition (I'm working on that)
- so just be fair to one another

## How to build
- `npm install`
- `grunt`
    - builds with sourcemaps and starts a watcher
- `grunt build`
    - clean build
    
## To do
- implement win condition
- maybe nicer visuals?

## Questions?
- hallo@iamschulz.de